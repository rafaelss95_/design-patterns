package behavioral.Command;

public interface IPaymentCommand {

    void processPurchase(Purchase purchase);
}
