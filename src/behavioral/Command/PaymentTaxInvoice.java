package behavioral.Command;

public class PaymentTaxInvoice implements IPaymentCommand {

    @Override
    public void processPurchase(Purchase purchase) {
        System.out.println("Boleto bancário criado!\n" + purchase.getInfoNote());
    }
}
