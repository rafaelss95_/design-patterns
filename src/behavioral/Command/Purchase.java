package behavioral.Command;

public class Purchase {

    private static int STATIC_ID;
    protected int taxInvoiceId;
    protected String shopName;
    protected double totalValue;

    public Purchase(String shopName) {
        this.shopName = shopName;
        taxInvoiceId = ++STATIC_ID;
    }

    public void setValue(double value) {
        this.totalValue = value;
    }

    public String getInfoNote() {
        return "Boleto bancário nº: " + taxInvoiceId + "\nLoja: " + shopName
                + "\nValor: " + totalValue + "\n";
    }
}
