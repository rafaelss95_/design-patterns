package behavioral.Command;

public class Main {

    public static void main(String[] args) {
        Shop shop = new Shop("USA Shop");
        shop.makePurchase(999.00, new PaymentCreditCard());
        shop.makePurchase(49.00, new PaymentTaxInvoice());
        shop.makePurchase(99.00, new PaymentDebitCard());

        shop = new Shop("Canada Shop");
        shop.makePurchase(19.00, new PaymentCreditCard());
    }
}
