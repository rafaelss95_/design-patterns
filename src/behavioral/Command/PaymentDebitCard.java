package behavioral.Command;

public class PaymentDebitCard implements IPaymentCommand {

    @Override
    public void processPurchase(Purchase purchase) {
        System.out.println("Compra efetuada!\n" + purchase.getInfoNote());
    }
}
