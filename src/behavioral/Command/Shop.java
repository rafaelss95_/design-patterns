package behavioral.Command;

public class Shop {

    protected String shopName;

    public Shop(String name) {
        shopName = name;
    }

    public void makePurchase(double value, IPaymentCommand formOfPayment) {
        Purchase purchase = new Purchase(shopName);
        purchase.setValue(value);
        formOfPayment.processPurchase(purchase);
    }
}
