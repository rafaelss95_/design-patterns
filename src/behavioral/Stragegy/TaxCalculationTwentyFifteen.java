package behavioral.Stragegy;

public class TaxCalculationTwentyFifteen implements ICalculationStrategy {

    @Override
    public double salaryWithTax(Employee employee) {
        double salary = employee.getBaseSalary();
        return salary > 3500 ? salary * .8 : salary * .85;
    }
}
