package behavioral.Stragegy;

public class Employee {

    public static final int DEVELOPER = 1;
    public static final int MANAGER = 2;
    public static final int DBA = 3;
    protected double baseSalary;
    protected int office;
    protected ICalculationStrategy calculationStrategy;

    public Employee(int office, double baseSalary) {
        this.baseSalary = baseSalary;
        switch (office) {
            case DEVELOPER:
                calculationStrategy = new TaxCalculationFifteenTen();
                break;
            case DBA:
                calculationStrategy = new TaxCalculationFifteenTen();
                break;
            case MANAGER:
                calculationStrategy = new TaxCalculationTwentyFifteen();
                break;
            default:
                throw new RuntimeException("Office not found");
        }
    }

    public double salaryWithTax() {
        return calculationStrategy.salaryWithTax(this);
    }

    public double getBaseSalary() {
        return baseSalary;
    }
}
