package behavioral.Stragegy;

public class Main {

    public static void main(String[] args) {
        getInfo(new Employee(Employee.DEVELOPER, 2100));
        getInfo(new Employee(Employee.DBA, 1700));
    }

    private static void getInfo(Employee employee) {
        System.out.println(employee.salaryWithTax());
    }
}
