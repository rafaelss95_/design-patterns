package behavioral.Stragegy;

interface ICalculationStrategy {

    double salaryWithTax(Employee employee);
}
