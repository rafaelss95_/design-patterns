package behavioral.Stragegy;

public class TaxCalculationFifteenTen implements ICalculationStrategy {

    @Override
    public double salaryWithTax(Employee employee) {
        double salary = employee.getBaseSalary();
        return salary > 2000 ? salary * .85 : salary * .9;
    }
}
