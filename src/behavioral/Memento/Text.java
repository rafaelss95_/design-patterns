package behavioral.Memento;

public class Text {

    protected String text;
    TextCareTaker caretaker;

    public Text() {
        caretaker = new TextCareTaker();
        text = new String();
    }

    public void writeText(String newText) {
        caretaker.addElement(new TextMemento(text));
        text += newText;
    }

    public void undoWriting() {
        text = caretaker.getLastSavedState().getSavedState();
    }

    public void showText() {
        System.out.println(text);
    }
}
