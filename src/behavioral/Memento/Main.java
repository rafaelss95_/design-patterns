package behavioral.Memento;

public class Main {

    public static void main(String[] args) {
        Text text = new Text();
        text.writeText("Primeira linha de texto\n");
        text.writeText("Segunda linha de texto\n");
        text.writeText("Terceira linha de texto\n");
        text.showText();
        text.undoWriting();
        text.showText();
        text.undoWriting();
        text.showText();
        text.undoWriting();
        text.showText();
        text.undoWriting();
        text.showText();
    }
}
