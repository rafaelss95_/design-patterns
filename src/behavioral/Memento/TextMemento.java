package behavioral.Memento;

public class TextMemento {

    protected String textStage;

    public TextMemento(String text) {
        textStage = text;
    }

    public String getSavedState() {
        return textStage;
    }
}
