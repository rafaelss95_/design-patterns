package behavioral.Memento;

import java.util.List;
import java.util.ArrayList;

public class TextCareTaker {

    protected List<TextMemento> stages;

    public TextCareTaker() {
        stages = new ArrayList();
    }

    public void addElement(TextMemento memento) {
        stages.add(memento);
    }

    public TextMemento getLastSavedState() {
        return stages.isEmpty()
                ? new TextMemento("")
                : stages.remove(stages.indexOf(stages.get(stages.size() - 1)));
    }
}
