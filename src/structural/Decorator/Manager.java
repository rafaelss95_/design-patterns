package structural.Decorator;

public class Manager extends PersonDecorator {

    public Manager(Person person) {
        super(person);
        name = "Mathew";
        salary = 1000.0;
    }
}
