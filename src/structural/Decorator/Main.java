package structural.Decorator;

public class Main {

    public static void main(String[] args) {
        Person person = new Employee();
        getInfo(person);

        person = new Manager(person);
        getInfo(person);
    }

    private static void getInfo(Person person) {
        System.out.println(person.getName() + " = " + person.getSalary());
    }
}
