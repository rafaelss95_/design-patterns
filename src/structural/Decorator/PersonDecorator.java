package structural.Decorator;

public abstract class PersonDecorator extends Person {

    Person person;

    public PersonDecorator(Person onePerson) {
        person = onePerson;
    }

    @Override
    public String getName() {
        return person.getName() + " + " + name;
    }

    @Override
    public double getSalary() {
        return person.getSalary() + salary;
    }
}
