package structural.Decorator;

public abstract class Person {

    String name;
    double salary;

    public String getName() {
        return name;
    }

    public double getSalary() {
        return salary;
    }
}
