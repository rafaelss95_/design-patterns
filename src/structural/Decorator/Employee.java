package structural.Decorator;

public class Employee extends Person {

    public Employee() {
        name = "Joshua";
        salary = 800.0;
    }
}
