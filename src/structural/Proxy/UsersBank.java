package structural.Proxy;

public class UsersBank {

    private final int TOTAL_USERS;
    private final int CONNECTED_USERS;

    public UsersBank() {
        TOTAL_USERS = (int) (Math.random() * 100);
        CONNECTED_USERS = (int) (Math.random() * 10);
    }

    public String getTotalUsers() {
        return "Total de usuários => " + TOTAL_USERS;
    }

    public String getConnectedUsers() {
        return "Usuários conectados => " + CONNECTED_USERS;
    }
}
