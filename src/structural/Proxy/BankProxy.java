package structural.Proxy;

public class BankProxy extends UsersBank {

    protected String user, password;

    public BankProxy(String user, String password) {
        this.user = user;
        this.password = password;
    }

    @Override
    public String getTotalUsers() {
        return hasPermission() ? super.getTotalUsers() : null;
    }

    @Override
    public String getConnectedUsers() {
        return hasPermission() ? super.getConnectedUsers() : null;
    }

    private boolean hasPermission() {
        return "admin".equals(user) && "admin".equals(password);
    }
}
