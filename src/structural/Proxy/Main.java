package structural.Proxy;

public class Main {

    public static void main(String[] args) {
        getInfo("Pessoa comum acessando... ", new BankProxy("Cracker", "1234"));
        getInfo("\nDBA acessando... ", new BankProxy("admin", "admin"));
    }

    private static void getInfo(String msg, UsersBank bank) {
        System.out.println(msg + "\n" + bank.getTotalUsers() 
                + "\n" + bank.getConnectedUsers());
    }
}
