package creational.builder.DealerShip;

public class Main {

    public static void main(String[] args) {
        getInfo(new DealerShip(new Mustang()));
        getInfo(new DealerShip(new RangeRover()));
    }

    private static void getInfo(DealerShip dealerShip) {
        dealerShip.buildCar();
        Car car = dealerShip.getCar();
        System.out.println(car + "\n");
    }
}
