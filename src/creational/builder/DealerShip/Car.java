package creational.builder.DealerShip;

public class Car {

    public double price;
    public String motor;
    public int manufactureYear;
    public String model;
    public String autoMaker;

    @Override
    public String toString() {
        return "Carro: " + model + "/" + autoMaker + "\nAno: "
                + manufactureYear + "\nMotor: " + motor + "\nPreço: " + price;
    }
}
