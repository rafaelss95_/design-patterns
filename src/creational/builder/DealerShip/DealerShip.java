package creational.builder.DealerShip;

public class DealerShip {

    protected CarBuilder autoMaker;

    public DealerShip(CarBuilder autoMaker) {
        this.autoMaker = autoMaker;
    }

    public void buildCar() {
        autoMaker.setPrice();
        autoMaker.setManufactureYear();
        autoMaker.setMotor();
        autoMaker.setModel();
        autoMaker.setAutoMaker();
    }

    public Car getCar() {
        return autoMaker.getCar();
    }
}
