package creational.builder.DealerShip;

public class RangeRover extends CarBuilder {

    @Override
    public void setPrice() {
        car.price = 80000.00;
    }

    @Override
    public void setMotor() {
        car.motor = "3.0";
    }

    @Override
    public void setManufactureYear() {
        car.manufactureYear = 2014;
    }

    @Override
    public void setModel() {
        car.model = "Range Rover";
    }

    @Override
    public void setAutoMaker() {
        car.autoMaker = "Land Rover";
    }
}
