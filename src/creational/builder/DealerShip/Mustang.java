package creational.builder.DealerShip;

public class Mustang extends CarBuilder {

    @Override
    public void setPrice() {
        car.price = 50000.00;
    }

    @Override
    public void setMotor() {
        car.motor = "2.0";
    }

    @Override
    public void setManufactureYear() {
        car.manufactureYear = 2015;
    }

    @Override
    public void setModel() {
        car.model = "GT-500";
    }

    @Override
    public void setAutoMaker() {
        car.autoMaker = "Mustang";
    }
}
