package creational.builder.DealerShip;

public abstract class CarBuilder {

    protected Car car = new Car();

    public abstract void setPrice();

    public abstract void setMotor();

    public abstract void setManufactureYear();

    public abstract void setModel();

    public abstract void setAutoMaker();

    public Car getCar() {
        return car;
    }
}
