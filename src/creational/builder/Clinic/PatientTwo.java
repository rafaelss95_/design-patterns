package creational.builder.Clinic;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class PatientTwo extends PatientBuilder {

    @Override
    public void setName() {
        patient.name = "Angelina Mason";
    }

    @Override
    public void setAddress() {
        patient.address = "Canada";
    }

    @Override
    public void setCPF() {
        patient.CPF = "304.903.400-07";
    }

    @Override
    public void setPhone() {
        patient.phone = "96432-8374";
    }

    @Override
    public void setBirthDate() {
        try {
            String date = "28/07/1975";
            patient.birthDate = new SimpleDateFormat("dd/MM/yyyy").parse(date);
        } catch (ParseException e) {
            System.err.println("Data no formato incorreto!");
        }
    }

    @Override
    public void setGender() {
        patient.gender = Gender.FEMALE;
    }
}
