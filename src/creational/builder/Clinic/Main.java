package creational.builder.Clinic;

public class Main {

    public static void main(String[] args) {
        getInfo(new Clinic(new PatientOne()));
        getInfo(new Clinic(new PatientTwo()));
    }

    private static void getInfo(Clinic clinic) {
        clinic.addPatient();
        Patient patient = clinic.getPatient();
        System.out.println(patient + "\n");
    }
}
