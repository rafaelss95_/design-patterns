package creational.builder.Clinic;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class PatientOne extends PatientBuilder {

    @Override
    public void setName() {
        patient.name = "Joe Rage";
    }

    @Override
    public void setAddress() {
        patient.address = "EUA";
    }

    @Override
    public void setCPF() {
        patient.CPF = "19230911076";
    }

    @Override
    public void setPhone() {
        patient.phone = "93849-7564";
    }

    @Override
    public void setBirthDate() {
        try {
            String date = "30/02/1999";
            patient.birthDate = new SimpleDateFormat("dd/MM/yyyy").parse(date);
        } catch (ParseException e) {
            System.err.println("Data no formato incorreto!");
        }
    }

    @Override
    public void setGender() {
        patient.gender = Gender.MALE;
    }
}
