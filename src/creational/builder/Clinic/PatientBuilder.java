package creational.builder.Clinic;

public abstract class PatientBuilder {

    protected Patient patient = new Patient();

    public abstract void setName();

    public abstract void setAddress();

    public abstract void setCPF();

    public abstract void setPhone();

    public abstract void setBirthDate();

    public abstract void setGender();

    public Patient getPatient() {
        return patient;
    }
}
