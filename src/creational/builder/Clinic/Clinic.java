package creational.builder.Clinic;

public class Clinic {

    protected PatientBuilder clinic;

    public Clinic(PatientBuilder clinic) {
        this.clinic = clinic;
    }

    public void addPatient() {
        clinic.setName();
        clinic.setAddress();
        clinic.setCPF();
        clinic.setPhone();
        clinic.setBirthDate();
        clinic.setGender();
    }

    public Patient getPatient() {
        return clinic.getPatient();
    }
}
