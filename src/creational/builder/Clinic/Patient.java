package creational.builder.Clinic;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Patient {

    public String name;
    public String address;
    public String CPF;
    public String phone;
    public Date birthDate;
    public Gender gender;

    @Override
    public String toString() {
        String genderOutput = gender.toString();

        return "Nome: " + name + "\nEndereço: " + address + "\nCPF: "
                + CPF + "\nFone: " + phone + "\nData de nascimento: "
                + new SimpleDateFormat("dd/MM/yyyy").format(birthDate)
                + "\nSexo: " + genderOutput
                .substring(0, 1)
                .toUpperCase()
                .concat(genderOutput.substring(1)
                        .toLowerCase());
    }
}
